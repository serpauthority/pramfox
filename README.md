<h1>Messy Play Activities Your Baby Can Enjoy At Home</h1>
<p>If you work from home or are perhaps a stay-at-home mom, keeping your baby entertained during the day can be challenging. Between feeding time, nap time and changing hundreds of diapers, you may feel burnt out and unable to come up with innovative ways to occupy your little one that don&rsquo;t involve screen time.&nbsp;</p>
<p>Messy play - a sensory type of play in which babies explore different materials and their properties - supports cognitive development, improves fine motor skills and promotes creativity (among many other benefits).&nbsp;</p>
<h2>But What About The Mess?!</h2>
<p>The idea of mess might be slightly overwhelming initially, but the developmental benefits outweigh the inconvenience of a little cleanup. It&rsquo;s in the name - messy play. Your baby will get messy, as is their immediate surroundings; therefore, containing the activity is vital. Consider purchasing a sizeable waterproof sheet, Tuff Tray or make use of the bathtub.</p>
<h2>Edible Messy Play</h2>
<p>Any parent will be familiar with a baby&rsquo;s tendency to put&nbsp;<em>everything&nbsp;</em>in their mouth. It is how they learn about their surroundings through sensory exploration. This is why using edible components in your messy play activity can put your mind at rest.&nbsp;</p>
<ul>
<li>Use food dye on cooked spaghetti to create a noodle rainbow. The bright colours stimulate your baby&rsquo;s visual capacity, and they&rsquo;ll love using their palmar grasp to pick up the strands.&nbsp;</li>
</ul>
<ul>
<li>Mix together toasted flour, cocoa powder and water into a consistency resembling mud. You can enrich this activity by adding your baby&rsquo;s favourite plastic barnyard animals for a muddy farm-themed frolic.</li>
</ul>
<ul>
<li>Next time you drain a can of chickpeas, save the water. Add &frac14; teaspoon of cream of tartar to the liquid and beat it like you would egg whites until you reach a stiff, foamy consistency. You can then colour this using food dye. Your baby will adore the soft, velvety texture.</li>
</ul>
<ul>
<li>For hot days, fill a large, shallow container with cool water and drop in slices of orange, lemon and lime (or any fruit you want). This will stimulate your baby&rsquo;s curiosity as they fish for the fruit slices and explore the zingy flavours.</li>
</ul>
<h2>Repurposing Household Essentials</h2>
<p>If you&rsquo;re in a pinch and want something sensory to entertain your baby quickly without having to make a trip to the store, here are some ideas for activities using things you might already have lying around the house.</p>
<ul>
<li>Fill a tray with uncooked rice, and add spoons and cups so your baby can experiment with filling, decanting and pouring. You can even dye the rice with paint or food dye. This also works with lentils or oats.</li>
</ul>
<ul>
<li>Fill a small container with water, add some small plastic toys or blocks, and then freeze it. Perfect for a cooling activity in the paddling pool, your baby can explore temperature as the ice slowly melts, releasing their toy.</li>
</ul>
<ul>
<li>Squirt some shaving foam into your <strong><em><span style="text-decoration: underline;"><a href="https://pramfox.com/collections/baby-bath-tubs">baby bath tub</a></span></em></strong> and add a few drops of food dye. Provide your baby with some paint brushes so they can swirl the colour into the foam. You could also squirt the foam onto a flat surface and use it as a mark-making activity.</li>
</ul>
<h2>Dough, Slime and Sand</h2>
<p>Store-bought playdough and slime can be pricey and have a very limited shelf life. Save yourself the bother and make it at home - it&rsquo;s far easier than you might think.</p>
<ul>
<li>A simple playdough can be made using flour, water and a drop of oil. Adding salt to make salt dough will extend its life, and you can include food dye or non-toxic paint to colour it. Providing your baby with blunt cookie cutters to make shapes will strengthen numerous muscles in their hands, arms and shoulders.</li>
</ul>
<ul>
<li>The internet is absolutely heaving with homemade slime recipes so take your pick; however, ensure all of the ingredients are safe for your baby - for example, you&rsquo;ll want to avoid the recipes that use borax. Your baby will love the stretchy, gooey texture squishing between their fingers and toes.</li>
</ul>
<ul>
<li>You can make a super simple sensory sandbox by blending up the contents of a cheap box of cereal. Kinetic sand is great fun but expensive to buy, so make your own with eight parts flour and one part baby oil.&nbsp;</li>
</ul>
<h3>Resources:</h3>
<ul>
<li><a href="https://pramfox.hashnode.dev/top-10-baby-shower-gifts">Baby Bedding - Hashnode</a></li>
<li><a href="https://publicationfirst.news.blog/2022/08/04/baby-skin-care-basics/">Baby Bath - News.blog</a></li>
<li><a href="https://www.smore.com/t7kbu-family-activities-in-singapore">Baby Changing Tables - Smore</a></li>
</ul>
